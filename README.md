# WoolTree

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)  [![forthebadge](https://forthebadge.com/images/badges/powered-by-coffee.svg)](https://forthebadge.com)  

Application web de partage de liens, essentiellement pour les réseaux sociaux.

## Pour commencer

Cette application de partage de liens s'apelle WoolTree, car à elle à été conçue initialement pour accompagner le lancement du site [www.petite-moutaine.fr](https://www.petite-miutaine.fr)

## Pré-requis

- Serveur hôte Linux (Debian/Ubuntu)
- Base de donnée MySql/MariaDB
- Docker
- Docker-compose

## Installation

L'application est fournie avec les environnements Docker de developemment et de production.

### Installation en developemment

wip

### Installation en production

wip

## Fabriqué avec

Programmes/logiciels/ressources que vous avez utilisé pour développer votre projet

* [Symfony](https://symfony.com/) - Framework PHP
* [Docker](https://www.docker.com/) - Virtualisation en contenaires
* [wip](wip) - WIP

## Auteurs

* **Yoan Bernabeu** _alias_ [@yoanbernabeu](https://gitlab.com/yoan.bernabeu)

## License

Ce projet est sous licence ``MIT`` - voir le fichier [LICENSE](LICENSE) pour plus d'informations