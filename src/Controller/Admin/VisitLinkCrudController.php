<?php

namespace App\Controller\Admin;

use App\Entity\VisitLink;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

class VisitLinkCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return VisitLink::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            DateTimeField::new('visitedOn'),
            AssociationField::new('link')
        ];
    }
}
