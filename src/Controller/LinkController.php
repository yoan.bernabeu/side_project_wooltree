<?php

namespace App\Controller;

use App\Entity\Link;
use App\Entity\Newsletter;
use App\Entity\User;
use App\Form\LinkFormType;
use App\Form\NewsletterType;
use App\Repository\LinkRepository;
use App\Service\LinkService;
use App\Service\NewsletterService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class LinkController extends AbstractController
{
    /**
     * @Route("/{slug}", name="link", priority=-1)
     */
    public function index(User $user, LinkRepository $linkRepository, Request $request, NewsletterService $newsletterService, TranslatorInterface $translator)
    {
        $links = $linkRepository->findByUserAndIsActivated($user);

        $newsletter = new Newsletter();

        $form = $this->createForm(NewsletterType::class, $newsletter);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newsletter= $form->getData();
            
            $addMail = $newsletterService->addMail($newsletter, $user);

            if ($addMail) {
                $this->addFlash('success', $translator->trans('You are now subscribed to our newsletter'));
            }

            if (!$addMail) {
                $this->addFlash('error', $translator->trans('This address is already subscribed'));
            }
        }

        return $this->render('link/index.html.twig', [
            'user'   => $user,
            'links'  => $links,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/link/redirect/{slug}", name="link_redirect")
     */
    public function link(Link $link, LinkService $linkService)
    {
        return $this->redirect($linkService->redirect($link));
    }

    /**
     * @Route("/myprofile/link/delete/{slug}", name="link_delete")
     */
    public function deleteLink(Link $link, LinkService $linkService, TranslatorInterface $translator)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $action = $linkService->delete($link);

        if ($action) {
            $this->addFlash('success', $translator->trans('Successful deletion !'));
        }

        if (!$action) {
            $this->addFlash('danger', $translator->trans('Impossible deletion !'));
        }

        return  $this->redirectToRoute('user_profil');
    }

    /**
     * @Route("/myprofile/link/create", name="link_create")
     */
    public function createLink(Request $request, LinkService $linkService, TranslatorInterface $translator)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $link = new Link();
        $link->setPublishFrom(new \DateTime('now', new \DateTimeZone('Europe/Paris')));

        $form = $this->createForm(LinkFormType::class, $link);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $link = $form->getData();
    
            $linkService->create($link, $this->getUser());

            $this->addFlash('success', $translator->trans('Your link is added to your list!'));
            
            return $this->redirectToRoute('user_profil');
        }
    
        return $this->render('link/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/myprofile/link/visibility/{slug}", name="link_visibility")
     */
    public function visibility(Link $link, LinkService $linkService)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $linkService->changeVisibility($link);

        return  $this->redirectToRoute('user_profil');
    }

    /**
     * @Route("/myprofile/link/edit/{slug}", name="link_edit")
     */
    public function editLink(Link $link, Request $request, LinkService $linkService, TranslatorInterface $translator)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(LinkFormType::class, $link);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $link = $form->getData();
    
            $linkService->create($link, $this->getUser());

            $this->addFlash('success', $translator->trans('Your link is updated !'));
            
            return $this->redirectToRoute('user_profil');
        }
    
        return $this->render('link/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/myprofile/link/stats/{slug}", name="link_stats")
     */
    public function stats(Link $link, LinkService $linkService)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
  
        $stats = $linkService->countVisitPerDay($link);

        $dates = [];
        $count = [];

        foreach ($stats as $stat) {
            $dates[] = $stat['date'];
            $count[] = $stat['count'];
        }

        return $this->render('link/stats.html.twig', [
            'dates' => json_encode($dates),
            'count' => json_encode($count),
        ]);
    }

    /**
     * @Route("/myprofile/link/up/{slug}", name="link_up")
     */
    public function upLink(Link $link, LinkService $linkService)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $linkService->upScale($link);

        return  $this->redirectToRoute('user_profil');
    }

    /**
     * @Route("/myprofile/link/down/{slug}", name="link_down")
     */
    public function downLink(Link $link, LinkService $linkService)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $linkService->downScale($link);

        return  $this->redirectToRoute('user_profil');
    }
}
