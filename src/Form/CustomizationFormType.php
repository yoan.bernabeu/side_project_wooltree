<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomizationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pseudo')
            ->add('newsletterIsActived', ChoiceType::class, [
                'choices'  => [
                    'No' => false,
                    'Yes' => true,
                ],
                'label' => 'Newsletter activation'
            ])
            ->add('color', ChoiceType::class, [
                'choices'  => [
                    'Default' => null,
                    'Pastel' => '_pastel',
                    'Petite Moutaine' => '_petitemoutaine',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
