<?php

namespace App\Repository;

use App\Entity\Link;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Link|null find($id, $lockMode = null, $lockVersion = null)
 * @method Link|null findOneBy(array $criteria, array $orderBy = null)
 * @method Link[]    findAll()
 * @method Link[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Link::class);
    }

    // /**
    //  * @return Link[] Returns an array of Link objects
    //  */
    
    public function findByUserAndIsActivated($user)
    {
        return $this->createQueryBuilder('link')
            ->andWhere('link.user = :user')
            ->andWhere('link.IsActivated = true')
            ->andWhere('link.isArchived = false')
            ->andWhere('link.publishFrom < :now')
            ->setParameter('now', new \DateTime('now', new \DateTimeZone('Europe/Paris')))
            ->setParameter('user', $user)
            ->orderBy('link.scale', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByUserAndIsNotAchived($user)
    {
        return $this->createQueryBuilder('link')
            ->andWhere('link.user = :user')
            ->andWhere('link.isArchived = false')
            ->setParameter('user', $user)
            ->orderBy('link.scale', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findLastLinkByUser($user): ?Link
    {
        return $this->createQueryBuilder('link')
            ->andWhere('link.user = :user')
            ->setParameter('user', $user)
            ->orderBy('link.CreatedAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
