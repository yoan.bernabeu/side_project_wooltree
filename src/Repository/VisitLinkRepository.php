<?php

namespace App\Repository;

use App\Entity\Link;
use App\Entity\VisitLink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VisitLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method VisitLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method VisitLink[]    findAll()
 * @method VisitLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitLinkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VisitLink::class);
    }

    public function countVisitByLink(Link $link)
    {
        return $this->createQueryBuilder('visitLink')
            ->select("DATE_FORMAT(visitLink.visitedOn, '%Y-%m-%d') AS date,  count(visitLink.id) AS count")
            ->groupBy('date')
            ->andWhere('visitLink.link = :link')
            ->setParameter('link', $link)
            ->getQuery()
            ->getResult();
        ;
    }
}
