<?php

namespace App\Service;

use App\Entity\Link;
use App\Entity\User;
use App\Entity\VisitLink;
use App\Repository\VisitLinkRepository;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Boolean;

class LinkService
{
    private $em;

    private $visitLinkRepository;

    public function __construct(EntityManagerInterface $em, VisitLinkRepository $visitLinkRepository)
    {
        $this->em = $em;
        $this->visitLinkRepository = $visitLinkRepository;
    }


    public function redirect(Link $link): String
    {
        $visit = new VisitLink();
        $visit->setLink($link)
              ->setVisitedOn(new DateTime('now', new DateTimeZone('Europe/Paris')));
        
        $this->em->persist($visit);
        $this->em->flush();
        
        return $link->getLink();
    }

    public function delete(Link $link)
    {
        $link->setIsArchived(true);

        $this->em->persist($link);
        $this->em->flush();
        
        return true;
    }

    public function create(Link $link, User $user): void
    {
        $link->setUser($user)
             ->setCreatedAt(new \DateTime('now'))
             ->setIsActivated(true)
             ->setIsArchived(false)
             ->setScale(0);

        $this->em->persist($link);
        $this->em->flush();
    }

    public function changeVisibility(Link $link): void
    {
        $link->setIsActivated(!$link->getIsActivated());

        $this->em->persist($link);
        $this->em->flush();
    }

    public function countVisitPerDay(Link $link): array
    {
        return $this->visitLinkRepository->countVisitByLink($link);
    }

    public function upScale(Link $link): void
    {
        $link->setScale($link->getScale() + 1);

        $this->em->persist($link);
        $this->em->flush();
    }

    public function downScale(Link $link): void
    {
        $link->setScale($link->getScale() - 1);

        $this->em->persist($link);
        $this->em->flush();
    }
}
