<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\UserAvatar;
use Doctrine\ORM\EntityManagerInterface;

class UserService
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    public function edit(User $user): void
    {
        $this->em->persist($user);
        $this->em->flush();
    }

    public function addAvatar(UserAvatar $userAvatar): UserAvatar
    {
        $this->em->persist($userAvatar);
        $this->em->flush();

        return $userAvatar;
    }

    public function setAvatar(UserAvatar $userAvatar, User $user): void
    {
        $user->setImageName($userAvatar->getImageName());
        $this->em->persist($user);
        $this->em->flush();
    }
}
