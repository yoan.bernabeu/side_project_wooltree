<?php

namespace App\Tests;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FunctionalTest extends WebTestCase
{
    public function testShouldDisplatyTheHomePage()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'WoolTree');
    }

    public function testShouldDisplatyTheLoginPage()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Connectez-vous');
    }

    public function testShouldDisplatyTheRegisterPage()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/register');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'S\'inscrire');
    }

    public function testShouldDisplatyTheResetPasswordPage()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/reset-password');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Remise à zéro de votre mot de passe');
    }

    public function testShouldDisplatyTheWoolTree()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/test');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', '@test');
    }

    public function testShouldRefuseToLogIn()
    {
        $client = static::createClient();
        $client->request('GET', '/login');

        $user = new User();
        $user->setEmail('test@functional.com')
             ->setPassword('password');

        $client->loginUser($user);

        $this->assertResponseStatusCodeSame(200);
        $this->assertSelectorTextContains('body', 'Connectez-vous');
    }

    public function testShouldDisplayTheDashboard()
    {
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);

        $user = $userRepository->findOneByEmail('test@test.com');

        $client->loginUser($user);

        $client->request('GET', '/myprofile');

        $this->assertResponseIsSuccessful();

        $this->assertResponseStatusCodeSame(200);

        $this->assertSelectorTextContains('body', 'Votre tableau de bord');
    }

    public function testShouldDisplayTheAddLinkPage()
    {
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);

        $user = $userRepository->findOneByEmail('test@test.com');

        $client->loginUser($user);

        $client->request('GET', '/myprofile/link/create');

        $this->assertResponseIsSuccessful();

        $this->assertResponseStatusCodeSame(200);

        $this->assertSelectorTextContains('body', 'Ajouter un lien');
    }

    public function testShouldDisplayTheCustomizationPage()
    {
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);

        $user = $userRepository->findOneByEmail('test@test.com');

        $client->loginUser($user);

        $client->request('GET', '/myprofile/customization');

        $this->assertResponseIsSuccessful();

        $this->assertResponseStatusCodeSame(200);

        $this->assertSelectorTextContains('body', 'Personnalisation');
    }

    public function testShouldDisplayTheAvatarPage()
    {
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);

        $user = $userRepository->findOneByEmail('test@test.com');

        $client->loginUser($user);

        $client->request('GET', '/myprofile/avatar');

        $this->assertResponseIsSuccessful();

        $this->assertResponseStatusCodeSame(200);

        $this->assertSelectorTextContains('body', 'Personnalisation');
    }

    public function testShouldDisplayTheLoginPageAfterLogout()
    {
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);

        $user = $userRepository->findOneByEmail('test@test.com');

        $client->loginUser($user);

        $client->request('GET', '/logout');

        $client->followRedirects();

        $this->assertResponseStatusCodeSame(302);
    }
}
