<?php

namespace App\Tests;

use App\Entity\Link;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class LinkTest extends TestCase
{
    public function testLinkTrue()
    {
        $link = new Link();

        $user = new User();

        $datetime = new \DateTime();

        $link->setName('test')
             ->setLink('test')
             ->setCreatedAt($datetime)
             ->setSlug('test')
             ->setIsActivated(true)
             ->setUser($user)
             ->setLogo('logo');

        $this->assertTrue($link->getName() === 'test');
        $this->assertTrue($link->getLink() === 'test');
        $this->assertTrue($link->getCreatedAt() === $datetime);
        $this->assertTrue($link->getSlug() === 'test');
        $this->assertTrue($link->getIsActivated() === true);
        $this->assertTrue($link->getUser() === $user);
        $this->assertTrue($link->getLogo() === 'logo');
    }

    public function testLinkFalse()
    {
        $link = new Link();

        $user = new User();

        $datetime = new \DateTime();

        $link->setName('test')
             ->setLink('test')
             ->setCreatedAt($datetime)
             ->setSlug('test')
             ->setIsActivated(true)
             ->setUser($user)
             ->setLogo('logo');

        $this->assertFalse($link->getName() === 'false');
        $this->assertFalse($link->getLink() === 'false');
        $this->assertFalse($link->getCreatedAt() === new DateTime());
        $this->assertFalse($link->getSlug() === 'false');
        $this->assertFalse($link->getIsActivated() === false);
        $this->assertFalse($link->getUser() === new User());
        $this->assertFalse($link->getLogo() === 'false');
    }

    public function testLinkEmpty()
    {
        $link = new Link();

        $this->assertEmpty($link->getName());
        $this->assertEmpty($link->getLink());
        $this->assertEmpty($link->getCreatedAt());
        $this->assertEmpty($link->getSlug());
        $this->assertEmpty($link->getIsActivated());
        $this->assertEmpty($link->getUser());
        $this->assertEmpty($link->getLogo());
    }
}
