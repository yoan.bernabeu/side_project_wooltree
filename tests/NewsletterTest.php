<?php

namespace App\Tests;

use App\Entity\Newsletter;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class NewsletterTest extends TestCase
{
    public function testNewsletterTrue()
    {
        $newsletter = new Newsletter();

        $user = new User();

        $newsletter->setUser($user)
                   ->setEmail('test@test.com');

        $this->assertTrue($newsletter->getUser() === $user);
        $this->assertTrue($newsletter->getEmail() === 'test@test.com');
    }

    public function testNewsletterFalse()
    {
        $newsletter = new Newsletter();

        $user = new User();

        $newsletter->setUser($user)
                   ->setEmail('test@test.com');

        $this->assertFalse($newsletter->getUser() === new User());
        $this->assertFalse($newsletter->getEmail() === 'false@false.com');
    }

    public function testNewsletterEmpty()
    {
        $newsletter = new Newsletter();

        $this->assertEmpty($newsletter->getUser());
        $this->assertEmpty($newsletter->getEmail());
    }
}
