<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testUserTrue()
    {
        $user = new User();

        $user->setEmail('test@test.com')
             ->setPassword('password')
             ->setPseudo('pseudo')
             ->setSlug('slug')
             ->setIsVerified(true)
             ->setColor('color')
             ->setImageName('image')
             ->setNewsletterIsActived(true);

        $this->assertTrue($user->getEmail() === 'test@test.com');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getPseudo() === 'pseudo');
        $this->assertTrue($user->getSlug() === 'slug');
        $this->assertTrue($user->isVerified() === true);
        $this->assertTrue($user->getColor() === 'color');
        $this->assertTrue($user->getImageName() === 'image');
        $this->assertTrue($user->getNewsletterIsActived() === true);
    }

    public function testUserFalse()
    {
        $user = new User();

        $user->setEmail('test@test.com')
             ->setPassword('password')
             ->setPseudo('pseudo')
             ->setSlug('slug')
             ->setIsVerified(true)
             ->setColor('color')
             ->setImageName('image')
             ->setNewsletterIsActived(true);

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getPseudo() === 'false');
        $this->assertFalse($user->getSlug() === 'false');
        $this->assertFalse($user->isVerified() === 'false');
        $this->assertFalse($user->getColor() === 'false');
        $this->assertFalse($user->getImageName() === 'false');
        $this->assertFalse($user->getNewsletterIsActived() === 'false');
    }

    public function testUserEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getPseudo());
        $this->assertEmpty($user->getSlug());
        $this->assertEmpty($user->isVerified());
        $this->assertEmpty($user->getColor());
        $this->assertEmpty($user->getImageName());
        $this->assertEmpty($user->getNewsletterIsActived());
    }
}
