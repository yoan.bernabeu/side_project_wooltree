<?php

namespace App\Tests;

use App\Entity\Link;
use App\Entity\VisitLink;
use PHPUnit\Framework\TestCase;

class VisitLinkTest extends TestCase
{
    public function testVisitLinkTrue()
    {
        $visitLink = new VisitLink();

        $link = new Link();

        $datetime = new \DateTime();

        $visitLink->setLink($link)
                  ->setVisitedOn($datetime);

        $this->assertTrue($visitLink->getLink() === $link);
        $this->assertTrue($visitLink->getVisitedOn() === $datetime);
    }

    public function testVisitLinkFalse()
    {
        $visitLink = new VisitLink();

        $link = new Link();

        $datetime = new \DateTime();

        $visitLink->setLink($link)
                  ->setVisitedOn($datetime);

        $this->assertFalse($visitLink->getLink() === new Link());
        $this->assertFalse($visitLink->getVisitedOn() === new \DateTime());
    }

    public function testVisitLinkEmpty()
    {
        $visitLink = new VisitLink();

        $this->assertEmpty($visitLink->getLink());
        $this->assertEmpty($visitLink->getVisitedOn());
    }
}
